# Backup bitwarden data

To deal with 2 auth factors we have to handle this avoiding login again.
Indeed, 2auth is only need on login and not on unlock process.
So a unlock with a sync process will do the work.

The only way that could/must fail is when bitwarden server has a to revokate all session key.
Revokated session key will block the ability to sync with the server again.

## Env settings

The following env vars allow you to customize the behavior of backup script

### SERVER

Allow to set a different server. If it's empty, it's the default bitwarden server
Could be a `https://server.url/` or a `server.url` also

### EMAIL

Set the email for the login to the server. If it's empty, it won't work

### PASSWORD

Set the password for the login to the server. If it's empty, it won't work

### BACKUP_FILENAME_PREFIX

It's usefull when you backup more than one account into the same directory.
If set, will replace the `bitwarden-` usual prefix

### PUID

Allow you to run the backup script as a specific userid

### PGID

Allow you to run the backup script as a specific groupid

## How to deal with the 2auth process (dev process at least)

You'll need to start with the following command that can handle interactive tty

```bash
docker-compose run backup-test /backup-bitwarden.sh
```

And all the following will use data from volumes/cache to start usually

```bash
docker-compose up
```
