FROM node:14-alpine

LABEL maintainer="lgnap+docker@helpcomputer.org"

RUN npm install -g @bitwarden/cli

RUN apk add --no-cache \
	jq \
	xz \
	gpgme \
	file \
	shadow \
	su-exec

# If not provided will default to the "classic" server
ENV SERVER ''

# Must be provided to get a backup
ENV EMAIL ''
ENV PASSWORD ''

# If set will replace the bitwarden- usual prefix
ENV BACKUP_FILENAME_PREFIX ''

USER node

# setup to avoid 'Inappropriate ioctl for device'
RUN mkdir ~/.gnupg
RUN chmod 700 ~/.gnupg
RUN echo 'use-agent' > ~/.gnupg/gpg.conf
RUN echo 'pinentry-mode loopback' >> ~/.gnupg/gpg.conf
RUN echo 'allow-loopback-pinentry' > ~/.gnupg/gpg-agent.conf

# one path to extract backup, the second to cache bitwaerden data
VOLUME ["/backup", "/home/node/.config"]

USER root

COPY entrypoint.sh /
COPY backup-bitwarden.sh /

ENTRYPOINT [ "/entrypoint.sh" ]

CMD ["/backup-bitwarden.sh"]
