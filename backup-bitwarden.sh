#! /bin/sh

set -e
set -o pipefail

if [ -n "$SERVER" ]
then
	bw config server "$SERVER"
fi

if [ -z "$EMAIL" ] || [ -z "$PASSWORD" ]
then
	echo "You've to provide a EMAIL and a PASSWORD to get bitwarden backup" >&2
	exit 1
fi

prefix_backup='bitwarden-'

if [ -n "$BACKUP_FILENAME_PREFIX" ]
then
	prefix_backup="$BACKUP_FILENAME_PREFIX-"
fi

echo "$prefix_backup"

backup_file=/backup/${prefix_backup}$(date +%Y-%m-%d-%H%M%S).tar.xz

# retrieve session and register it to avoid type it again for whole script
echo "Try to login ..."
set +e
SESSION_KEY=$(bw login "$EMAIL" "$PASSWORD" --raw)
LOGIN_STATUS=$?
set -e

# when data are already stored through volumes the next login will fail, but fallback in unlock in this case
# this is usefull when login process need a 2auth factor that can't be provided through unattended process
if [ ${LOGIN_STATUS} -eq 0 ]
then
	echo "Success login, continue"
else
	echo "Error on login, try unlock: '$SESSION_KEY'"
	SESSION_KEY=$(bw unlock "$PASSWORD" --raw)
fi

export BW_SESSION="$SESSION_KEY"

echo "Sync with server"
bw sync
echo "Synced with server"

# create backup directory if missing
backup_directory="/tmp/bitwarden-backup"

if [ ! -d "${backup_directory}" ]; then
	mkdir "${backup_directory}"
fi

# export data through usual process
export_path=${backup_directory}/export.csv
bw export "$PASSWORD" --output "${export_path}"

# CHECK CONTENT OF NOTES, CHECK ATTACHMENTS

# List items with attachments
items=$(bw list items)
items_from_organization=$(echo "$items" | jq '.[] | select(.organizationId)')

# Get ids only
item_ids=$(echo "$items_from_organization" | jq --raw-output ".id")

# for each item from organization(s)
for item_id in ${item_ids}
do
	#create a directory
	item_directory=${backup_directory}/${item_id}
	item_path=$item_directory/dump.json

	if [ ! -d "${item_directory}" ]; then
		mkdir "${item_directory}"
	fi

	# filter the item from list
	item=$(echo "$items_from_organization" | jq "select(.id == \"${item_id}\")")

	# backup a dump into folder (not exported by default export)
	echo "$item" > "${item_path}"
	
	# grep all attachement ids for this item_id
	attachment_ids=$(echo "$item" | jq --raw-output '.attachments[]?.id')

	# for each attachement
	for attachment_id in ${attachment_ids}
	do
		attachment=$(echo "$item" | jq ".attachments[] | select(.id == \"${attachment_id}\")")
		attachment_filename=$(echo "${attachment}" | jq --raw-output ".fileName")
		attachment_path=${item_directory}/${attachment_filename}
		
		# if file not existing (avoid to download again an existing file)
		if [ ! -f "$attachment_path" ] 
		then
			bw get attachment "${attachment_id}" --output "${attachment_path}" --itemid "${item_id}"
		fi
	done
done

# package all files into a xz
cd "${backup_directory}" && tar cJf "${backup_file}" .

# encrypt symetric backup file
gpg --symmetric --passphrase "${PASSWORD}" --cipher-algo AES256 "${backup_file}"

# clean all temp files
rm "${backup_file}"
rm -rf "${backup_directory}"

# PROVIDE SOME DATA TO ALLOW EASY DECRYPTING
echo "This backup is ciphered using GPG symmetric encryption, please get it to use your backup"
echo
echo "The password used for GPG encryption is the same than provided for login to bitwarden"
echo "But you should known that because ..."
echo "OF COURSE YOU READ THAT CODE BEFORE RUN IT AGAINST YOUR PERSONAL SERVER/LOGIN/PASSWORD!"
echo 
echo 
echo "File produced to retrieve from mountpoint: "
ls -l "${backup_file}"*
file --brief "${backup_file}"*


# TODO
# * try to reimport exported data
# * cannot check size after dl :'( incorrectly reported by bw
