.PHONY=build run help
.DEFAULT_GOAL=help

IMAGE_DEV_NAME=bb-image
CONTAINER_NAME=bb-instance
ENV_FILE=perso.env

build: ## Build docker image
	docker build --tag $(IMAGE_DEV_NAME) .

clean: clean-dev clean-prod ## clean prod & dev volumes

clean-dev: ## clean dev volumes
	rm -rf volumes/dev

clean-prod: ## clean prod volumes
	rm -rf volumes/prod

run: ## Run docker image
	docker run \
	--tty \
	--interactive \
	--rm \
	--env-file $(ENV_FILE) \
	--volume "${PWD}/volumes/dev/backup:/backup" \
	--volume "${PWD}/volumes/dev/config:/home/node/.config" \
	--name  $(CONTAINER_NAME) \
	$(IMAGE_DEV_NAME)

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
