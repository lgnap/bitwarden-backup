#! /bin/sh

set -eo pipefail

PUID=${PUID:-1000}
PGID=${PGID:-1000}

groupmod -o -g "$PGID" node
usermod -o -u "$PUID" node

chown -R node:node /home/node/

exec su-exec node:node "$@"
